# Escuela de Yoga

## Descripcion

Nuestra pagina brinda toda la informacion necesaria para pertenecer a nuestra maravillosa escuela de Yoga donde encontraras nuestros servicios, el portafolio de cursos que ofrecemos, quienes somos nosotros, el equipo de profesionales que tenemos para ti y como contactarnos.

>Si quieres saber mas visitanos
[Pagina Web](https://thriving-cactus-865e25.netlify.app/index.html)

## Como clonar
> Para clonar el proyecto brindamos las siguientes 3 opciones.
 * **Clone with SSH:** 
 ```
 git@gitlab.com:darias05/darias05.git
 ```
 * **Clone with HTTPS:** 
 ```
 git@github.com:darias05/Curriculum_Viate.git
 ```

## Autores
> Dylan Arias Arenas

> Ginna Paola Tangarife

# Vista previa

Este modulo nos ofrece la oportunidad de ver una vista previa de la construcción de nuestra pagina Web.

## Inicio
![foto de inicio](./assets/img/plantilla/Home.jpeg)

## Servicios
![foto de servicios](./assets/img/plantilla/Services.jpeg)

## Portafolio
![foto de portafolio](./assets/img/plantilla/Portfolio.jpeg)

## Nosotros
![foto de nosotros](./assets/img/plantilla/About.jpeg)

## Equipo
![foto de equipo](./assets/img/plantilla/Team.jpeg)

## Contactanos
![foto de contactanos](./assets/img/plantilla/Contact.jpeg)

# Framework y plantillas

## Booststrap

Bootstrap es una biblioteca multiplataforma o conjunto de herramientas de código abierto para diseño de sitios y aplicaciones web.

![Booststrap](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/1200px-Bootstrap_logo.svg.png)

Tomado de https://es.wikipedia.org/wiki/Bootstrap_(framework)

Hay un archivo principal llamado bootstrap.css, que contiene una definición para todos los estilos utilizados. Básicamente, la estructura del framework se compone de dos directorios:

+ ==css==: contiene los archivos necesarios para la estilización de los elementos y una alternativa al tema original
+ ==js==: contiene la parte posterior del archivo bootstrap.js (original y minificado), responsable de la ejecución de aplicaciones de estilo que requieren manipulación interactiva.

### Funcionalidades de Booststrap
+ ==Diseño Responsive==: Permite que la adaptacion de la pagina se realice según el tipo de dispositivo utilizado. Para garantizar la responsividad, el framework funciona con:
  - La estilizacion del elemento <div>
  - El uso de class container
+ ==Biblioteca de componentes==: Tiene componentes que pueden ser usados para proporcionar una mejor interacción y perfeccionar la comunicación con el usuario.
  - Alertas
  - Carrusel
  - Barras de navegacion

## Plantilla Booststrap

> Aca encontraras la URL de la plantilla de referencia para la construcción de nuestra pagina Web [Ir a la plantilla](https://startbootstrap.com/theme/agency)